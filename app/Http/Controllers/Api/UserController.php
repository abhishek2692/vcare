<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User;
use App\UserDetails; 
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest; 
use Validator;

class UserController extends Controller 
{
public $successStatus = 200;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->user = Auth::guard('api')->user();
        $this->userDetails = new UserDetails();
    }

    /**
     * Display the auth user.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        $userDetails = User::with('userDetails')->where('id',$user['id'])->first();
        
        return $userDetails;
    }

    /**
     * Update the auth user's profile in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(UserRequest $request)
    {
        $validated = $request->validated();

        $this->user->name = $validated['name'];
        $this->user->phone_number = $validated['phone_number'];
        $this->user->save();

        $user_details = $this->userDetails->where('user_id',Auth::user()->id)->update([
            'father' => $validated['father'],
            'mother' => $validated['mother'],
            'wife' => $validated['wife'],
            'child' => $validated['child'],
            'address' => $validated['address']
        ]);
        
        $userDetails = User::with('userDetails')->where('id',Auth::user()->id)->first();
        
        return $userDetails;
    }
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
    
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
        ]);

        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }
        $input = $request->all(); 
                $input['password'] = bcrypt($input['password']); 
                $user = User::create($input); 
                $success['token'] =  $user->createToken('MyApp')-> accessToken; 
                $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus); 
    }
/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 


}