<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:191',
            'father'        => 'required|max:191',
            'mother'        => 'required|max:191',
            'child'         => 'required|max:191',
            'address'       => 'required|max:191',
            'wife'          => 'required|max:191',
            'phone_number'  => 'required|numeric|digits:10',
        ];
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required'    => 'Email is required!',
            'name.required'     => 'Name is required!',
            'password.required' => 'Password is required!'
        ];
    }
}
