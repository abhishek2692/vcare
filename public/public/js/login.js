(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["public/js/login"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Login.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Login.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      show: false,
      loggedIn: false,
      afterEmit: null,
      form: {
        email: null,
        password: null,
        errors: []
      },
      loading: false
    };
  },
  watch: {
    show: function show(_show) {
      this.$root.freeze = _show;
    }
  },
  created: function created() {
    var _this = this;

    this.$root.$on("show-login-modal", function () {
      var afterEmit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      _this.afterEmit = afterEmit;
      _this.show = true;
    });
    this.loggedIn = this.$root.loggedIn;
  },
  methods: {
    login: function login() {
      var _this2 = this;

      this.loading = true;
      var _this$form = this.form,
          email = _this$form.email,
          password = _this$form.password;
      axios.post("login", {
        email: email,
        password: password
      }).then(function (resp) {
        _this2.$root.loggedIn = true;
        _this2.loggedIn = _this2.$root.loggedIn;
        window.axios.defaults.headers.common['X-CSRF-TOKEN'] = resp.data.csrf_token;
        _this2.afterEmit !== null ? _this2.$root.$emit(_this2.afterEmit) : window.location = resp.data.redirect;
      })["catch"](function (error) {
        _this2.loading = false;
        var resp = error.response;

        if (resp) {
          if (resp.status == 422) {
            _this2.form.errors = resp.data.errors;
          }
        } else {
          console.log(error);
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return !_vm.loggedIn
    ? _c(
        "div",
        {
          class: [
            "flex flex-col items-center justify-center fixed top-0 left-0 z-50 w-full h-0 overflow-hidden",
            { "h-full overflow-auto": _vm.show }
          ],
          style: { background: "rgba(69, 65, 132, 0.25)" }
        },
        [
          _c(
            "div",
            {
              staticClass:
                "form-wrapper bg-white shadow-md w-full p-10 relative"
            },
            [
              _c(
                "button",
                {
                  staticClass:
                    "absolute top-0 right-0 m-3 bg-primary text-white px-3 py-2",
                  attrs: { disabled: _vm.loading },
                  on: {
                    click: function($event) {
                      _vm.show = false
                    }
                  }
                },
                [_c("i", { staticClass: "fa fa-times" })]
              ),
              _vm._v(" "),
              _c(
                "h5",
                {
                  staticClass:
                    "text-2xl font-normal text-primary text-center mt-6 mb-3"
                },
                [
                  _vm._v(
                    _vm._s(
                      _vm.afterEmit
                        ? "Please Login to Edcon Steel to Proceed"
                        : "Welcome Back!"
                    )
                  )
                ]
              ),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n        Please note: As we have recently updated our website, all of our existing customers MUST "
                ),
                _c(
                  "a",
                  {
                    staticClass: "tracking-wide hover:underline",
                    attrs: { href: _vm.$root.url + "/register" }
                  },
                  [_vm._v("register")]
                ),
                _vm._v(
                  " for a new Edcon Steel online account in order to continue using our online services. This is a one-time-only registration. We apologise for the inconvenience.\n      "
                )
              ]),
              _vm._v(" "),
              _vm.show
                ? _c(
                    "form",
                    {
                      attrs: { method: "POST" },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.login($event)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "mb-2" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.email,
                              expression: "form.email"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "email",
                            name: "email",
                            placeholder: "E-Mail Address",
                            required: "",
                            autocomplete: "email",
                            disabled: _vm.loading
                          },
                          domProps: { value: _vm.form.email },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.form, "email", $event.target.value)
                            }
                          }
                        }),
                        _vm._v(" "),
                        _vm.form.errors.email
                          ? _c(
                              "span",
                              {
                                staticClass: "text-red font-medium block",
                                attrs: { role: "alert" }
                              },
                              [_vm._v(_vm._s(_vm.form.errors.email[0]))]
                            )
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "mb-2" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.password,
                              expression: "form.password"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "password",
                            name: "password",
                            placeholder: "Password",
                            required: "",
                            autocomplete: "current-password",
                            disabled: _vm.loading
                          },
                          domProps: { value: _vm.form.password },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.form,
                                "password",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _vm.form.errors.passwords
                          ? _c(
                              "span",
                              {
                                staticClass: "text-red font-medium block",
                                attrs: { role: "alert" }
                              },
                              [_vm._v(_vm._s(_vm.form.errors.passwords[0]))]
                            )
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _vm._m(0),
                      _vm._v(" "),
                      _c("div", { staticClass: "mt-6" }, [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-primary w-full",
                            attrs: { type: "submit", disabled: _vm.loading }
                          },
                          [_vm._v("Login")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "mt-6" }, [
                        _c(
                          "a",
                          {
                            staticClass: "tracking-wide hover:underline",
                            attrs: { href: _vm.$root.url + "/register" }
                          },
                          [
                            _vm._v(
                              "\n          Don't Have a Free Account? Register Here\n         "
                            )
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "w-full mt-6" }, [
                        _c(
                          "a",
                          {
                            staticClass: "tracking-wide hover:underline",
                            attrs: { href: _vm.$root.url + "/password/reset" }
                          },
                          [_vm._v("Forgot Your Password?")]
                        )
                      ])
                    ]
                  )
                : _vm._e()
            ]
          )
        ]
      )
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mt-3 mb-2 flex items-center" }, [
      _c("input", {
        attrs: { type: "checkbox", name: "remember", id: "_remember" }
      }),
      _vm._v(" "),
      _c("label", { staticClass: "ml-1", attrs: { for: "_remember" } }, [
        _vm._v("Remember Me")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: ENOENT: no such file or directory, open 'C:\\xampp\\htdocs\\blog\\node_modules\\vue-loader\\lib\\runtime\\componentNormalizer.js'");

/***/ }),

/***/ "./resources/js/components/Login.vue":
/*!*******************************************!*\
  !*** ./resources/js/components/Login.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=6bdc8b8e& */ "./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/components/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Login.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/components/Login.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=6bdc8b8e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Login.vue?vue&type=template&id=6bdc8b8e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_6bdc8b8e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);