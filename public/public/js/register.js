(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["public/js/register"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Register.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Register.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      steps: 3,
      step: 1,
      stepClass: "mt-3 mb-2 font-serif text-gray-lighter text-sm tracking-wider font-semibold uppercase",
      areaCodes: [{
        text: "02",
        value: "02",
        call: true,
        sms: false
      }, {
        text: "03",
        value: "03",
        call: true,
        sms: false
      }, {
        text: "04",
        value: "04",
        call: true,
        sms: true
      }, {
        text: "07",
        value: "07",
        call: true,
        sms: false
      }, {
        text: "08",
        value: "08",
        call: true,
        sms: false
      }],
      selectedAreaCode: {
        text: "04",
        value: "04",
        call: true,
        sms: true
      },
      form: {
        message_type: null,
        area_code: null,
        phone_number: null,
        verification_code: null,
        first_name: null,
        last_name: null,
        email: null,
        email_confirmation: null,
        password: null,
        password_confirmation: null
      },
      loading: false,
      errors: [],
      phoneFocused: false,
      verificationSent: null
    };
  },
  methods: {
    sendSms: function sendSms() {
      this.form.message_type = "sms";
      this.sendVerificationCode();
    },
    makeCall: function makeCall() {
      this.form.message_type = "call";
      this.sendVerificationCode();
    },
    sendVerificationCode: function sendVerificationCode() {
      var _this = this;

      if (!this.$refs.sendVerificationForm.checkValidity()) {
        this.errors = {
          phone_number: ["Phone number field is required."]
        };
        return false;
      }

      this.errors = [];
      this.verificationSent = null;
      this.loading = true;
      var messageType = this.form.message_type;
      var areaCode = this.selectedAreaCode.value;
      this.form.area_code = areaCode;
      var phoneNumber = this.form.phone_number;
      axios.post("phone/send", {
        message_type: messageType,
        area_code: areaCode,
        phone_number: phoneNumber
      }).then(function (response) {
        _this.verificationSent = response.data.message;
        _this.step = 2;
      })["catch"](function (error) {
        if (error.response.status == 422) {
          _this.errors = error.response.data.errors;
          _this.step = 1;
        }
      })["finally"](function (_) {
        return _this.loading = false;
      });
    },
    verify: function verify() {
      var _this2 = this;

      this.errors = [];
      this.loading = true;
      var data = {
        area_code: this.form.area_code,
        phone_number: this.form.phone_number,
        verification_code: this.form.verification_code
      };
      axios.post("phone/verify", data).then(function (response) {
        var data = response.data.data;
        _this2.form.first_name = data.first_name;
        _this2.form.last_name = data.last_name;
        _this2.step = 3;
      })["catch"](function (error) {
        if (error.response.status == 422) _this2.errors = error.response.data.errors;
      })["finally"](function (_) {
        return _this2.loading = false;
      });
    },
    register: function register() {
      var _this3 = this;

      this.errors = [];
      this.loading = true;
      var data = this.form;
      axios.post("register", data).then(function (response) {
        window.location = response.data.redirect;
      })["catch"](function (error) {
        if (error.response.status == 422) _this3.errors = error.response.data.errors;
      })["finally"](function (_) {
        return _this3.loading = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Register.vue?vue&type=template&id=97358ae4&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Register.vue?vue&type=template&id=97358ae4& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "h5",
      { staticClass: "text-lg font-serif tracking-wider uppercase mb-3" },
      [_vm._v("Register")]
    ),
    _vm._v(" "),
    _vm.step == 1
      ? _c(
          "form",
          {
            ref: "sendVerificationForm",
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.sendVerificationCode($event)
              }
            }
          },
          [
            _c("p", [
              _vm._v(
                "Your Edcon Steel account allows you to access Edcon Steel’s services, place your online orders and more."
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "You can sign in to all Edcon Steel services with a single Edcon Steel account and password."
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Please note: As we have recently updated our website, all of our existing customers MUST register for a new Edcon Steel online account in order to continue using our online services. This is a one-time-only registration. We apologise for the inconvenience."
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "If you have any questions regarding your online account, Please contact us or give us a call on 1300 233 266 and our friendly staff will be happy to assist you."
              )
            ]),
            _vm._v(" "),
            _c("h3", { class: _vm.stepClass }, [
              _vm._v("Step 1 - Send verification code")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c("div", { staticClass: "flex items-center justify-center" }, [
                _c(
                  "div",
                  { staticClass: "form-select w-20 bg-gray-lightest" },
                  [
                    _c(
                      "select",
                      {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.selectedAreaCode,
                            expression: "selectedAreaCode"
                          }
                        ],
                        staticClass: "form-control pl-5",
                        on: {
                          change: function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.selectedAreaCode = $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          }
                        }
                      },
                      _vm._l(_vm.areaCodes, function(areaCode) {
                        return _c(
                          "option",
                          { key: areaCode.code, domProps: { value: areaCode } },
                          [_vm._v(_vm._s(areaCode.text))]
                        )
                      }),
                      0
                    )
                  ]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "w-full" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.phone_number,
                        expression: "form.phone_number"
                      }
                    ],
                    class: [
                      "form-control",
                      _vm.errors.phone_number
                        ? "border-red focus:border-red"
                        : ""
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "Phone Number",
                      required: "",
                      autofocus: "",
                      disabled: _vm.loading
                    },
                    domProps: { value: _vm.form.phone_number },
                    on: {
                      keydown: function($event) {
                        if (
                          !$event.type.indexOf("key") &&
                          _vm._k(
                            $event.keyCode,
                            "enter",
                            13,
                            $event.key,
                            "Enter"
                          )
                        ) {
                          return null
                        }
                        $event.preventDefault()
                      },
                      focus: function($event) {
                        _vm.phoneFocused = true
                      },
                      focusout: function($event) {
                        _vm.phoneFocused = false
                      },
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "phone_number", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _vm.errors.phone_number
                ? _c(
                    "span",
                    {
                      staticClass: "text-red font-medium block",
                      attrs: { role: "alert" }
                    },
                    [_vm._v(_vm._s(_vm.errors.phone_number[0]))]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.errors.phone
                ? _c(
                    "span",
                    {
                      staticClass: "text-red font-medium block",
                      attrs: { role: "alert" }
                    },
                    [_vm._v(_vm._s(_vm.errors.phone[0]))]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mt-6 flex justify-between" }, [
              _vm.selectedAreaCode.sms
                ? _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button", disabled: _vm.loading },
                      on: { click: _vm.sendSms }
                    },
                    [
                      _c("i", { staticClass: "fa fa-sms" }),
                      _vm._v("\n        Send SMS\n      ")
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-primary",
                  attrs: { type: "button", disabled: _vm.loading },
                  on: { click: _vm.makeCall }
                },
                [
                  _c("i", { staticClass: "fa fa-phone fa-rotate-90" }),
                  _vm._v("\n        Make Call\n      ")
                ]
              )
            ])
          ]
        )
      : _vm._e(),
    _vm._v(" "),
    _vm.step == 2
      ? _c(
          "form",
          {
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.verify($event)
              }
            }
          },
          [
            _c("p", [
              _vm._v("A verification code has been sent to your phone.")
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Please enter the verification code you received in the text field below."
              )
            ]),
            _vm._v(" "),
            _c("h3", { class: _vm.stepClass }, [
              _vm._v("Step 2 - Verification")
            ]),
            _vm._v(" "),
            _vm.verificationSent
              ? _c("message", {
                  staticClass: "my-6",
                  attrs: { type: "success", text: _vm.verificationSent }
                })
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.verification_code,
                    expression: "form.verification_code"
                  }
                ],
                class: [
                  "form-control",
                  _vm.errors.verification_code
                    ? "border-red focus:border-red"
                    : ""
                ],
                attrs: {
                  type: "text",
                  placeholder: "Verification Code",
                  required: "",
                  autofocus: "",
                  disabled: _vm.loading
                },
                domProps: { value: _vm.form.verification_code },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "verification_code", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.errors.verification_code
                ? _c(
                    "span",
                    {
                      staticClass: "text-red font-medium block",
                      attrs: { role: "alert" }
                    },
                    [_vm._v(_vm._s(_vm.errors.verification_code[0]))]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mt-6 flex justify-between" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary",
                  attrs: { type: "button", disabled: _vm.loading },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.step = 1
                    }
                  }
                },
                [_vm._v("Resend")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-primary",
                  attrs: { type: "submit", disabled: _vm.loading }
                },
                [_vm._v("Verify")]
              )
            ])
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _vm.step == 3
      ? _c(
          "form",
          {
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.register($event)
              }
            }
          },
          [
            _c("p", [
              _vm._v(
                "Your email address will be your username. Your email address will also be used as our primary means to contact you for any order enquiries or notifications."
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Please enter your full name, email address and a secure password."
              )
            ]),
            _vm._v(" "),
            _c("h3", { class: _vm.stepClass }, [_vm._v("Step 3 - Account")]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.first_name,
                    expression: "form.first_name"
                  }
                ],
                class: [
                  "form-control",
                  _vm.errors.first_name ? "border-red focus:border-red" : ""
                ],
                attrs: {
                  type: "text",
                  placeholder: "First Name",
                  required: "",
                  autocomplete: "first_name",
                  autofocus: ""
                },
                domProps: { value: _vm.form.first_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "first_name", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.errors.first_name
                ? _c(
                    "span",
                    {
                      staticClass: "text-red font-medium block",
                      attrs: { role: "alert" }
                    },
                    [_vm._v(_vm._s(_vm.errors.first_name[0]))]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.last_name,
                    expression: "form.last_name"
                  }
                ],
                class: [
                  "form-control",
                  _vm.errors.last_name ? "border-red focus:border-red" : ""
                ],
                attrs: {
                  type: "text",
                  placeholder: "Last Name",
                  required: "",
                  autocomplete: "last_name",
                  autofocus: ""
                },
                domProps: { value: _vm.form.last_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "last_name", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.errors.last_name
                ? _c(
                    "span",
                    {
                      staticClass: "text-red font-medium block",
                      attrs: { role: "alert" }
                    },
                    [_vm._v(_vm._s(_vm.errors.last_name[0]))]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.email,
                    expression: "form.email"
                  }
                ],
                class: [
                  "form-control",
                  _vm.errors.email ? "border-red focus:border-red" : ""
                ],
                attrs: {
                  type: "email",
                  placeholder: "Email Address",
                  required: "",
                  autocomplete: "email"
                },
                domProps: { value: _vm.form.email },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "email", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.errors.email
                ? _c(
                    "span",
                    {
                      staticClass: "text-red font-medium block",
                      attrs: { role: "alert" }
                    },
                    [_vm._v(_vm._s(_vm.errors.email[0]))]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.email_confirmation,
                    expression: "form.email_confirmation"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "email",
                  placeholder: "Confirm Email Address",
                  required: ""
                },
                domProps: { value: _vm.form.email_confirmation },
                on: {
                  paste: function($event) {
                    $event.preventDefault()
                  },
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "email_confirmation",
                      $event.target.value
                    )
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.password,
                    expression: "form.password"
                  }
                ],
                class: [
                  "form-control",
                  _vm.errors.password ? "border-red focus:border-red" : ""
                ],
                attrs: {
                  type: "password",
                  placeholder: "Password",
                  required: "",
                  autocomplete: "new-password"
                },
                domProps: { value: _vm.form.password },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "password", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.errors.password
                ? _c(
                    "span",
                    {
                      staticClass: "text-red font-medium block",
                      attrs: { role: "alert" }
                    },
                    [_vm._v(_vm._s(_vm.errors.password[0]))]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-2" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.password_confirmation,
                    expression: "form.password_confirmation"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "password",
                  placeholder: "Confirm Password",
                  required: "",
                  autocomplete: "new-password"
                },
                domProps: { value: _vm.form.password_confirmation },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "password_confirmation",
                      $event.target.value
                    )
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mt-6" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary",
                  attrs: { type: "submit", disabled: _vm.loading }
                },
                [_vm._v("Register")]
              )
            ])
          ]
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: ENOENT: no such file or directory, open 'C:\\xampp\\htdocs\\blog\\node_modules\\vue-loader\\lib\\runtime\\componentNormalizer.js'");

/***/ }),

/***/ "./resources/js/components/Register.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/Register.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Register.vue?vue&type=template&id=97358ae4& */ "./resources/js/components/Register.vue?vue&type=template&id=97358ae4&");
/* harmony import */ var _Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Register.vue?vue&type=script&lang=js& */ "./resources/js/components/Register.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Register.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Register.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/Register.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Register.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Register.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Register.vue?vue&type=template&id=97358ae4&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/Register.vue?vue&type=template&id=97358ae4& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Register.vue?vue&type=template&id=97358ae4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Register.vue?vue&type=template&id=97358ae4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_97358ae4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);