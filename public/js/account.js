(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/account"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Account.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Account.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: {
        editMode: false
      },
      address: {
        states: [],
        districts: [],
        places: [],
        district: null,
        place: null,
        editMode: false,
        loadingPlaces: false
      },
      currentAddress: {},
      userForm: {
        name: null,
        phone_number: null,
        avatar: null
      },
      addressForm: {},
      loading: true,
      updating: false,
      userErrors: [],
      addressErrors: []
    };
  },
  created: function created() {
    this.getUser(); // this.getStates();
    //this.getUserDetails();
  },
  methods: {
    getUser: function getUser() {
      var _this = this;

      axios("api/user").then(function (response) {
        _this.user = _objectSpread(_objectSpread({}, _this.user), response.data);
        _this.userdetails = _objectSpread(_objectSpread({}, _this.userdetails), response.data['user_details']);
        _this.userForm.name = response.data.name;
        _this.userForm.phone_number = response.data.phone_number;
        _this.userForm.avtar = response.data.avtar;
        _this.userForm.father = response.data['user_details'].father;
        _this.userForm.mother = response.data['user_details'].mother;
        _this.userForm.wife = response.data['user_details'].wife;
        _this.userForm.child = response.data['user_details'].child;
        _this.userForm.address = response.data['user_details'].address;
      });
    },
    uploadAvatar: function uploadAvatar() {
      alert();
    },
    updateUser: function updateUser() {
      var _this2 = this;

      this.updating = true;
      axios.put("api/user/profile", this.userForm).then(function (response) {
        _this2.user = _objectSpread(_objectSpread({}, _this2.user), response.data);
        _this2.userdetails = _objectSpread(_objectSpread({}, _this2.userdetails), response.data['user_details']);
        _this2.user.editMode = false;
      })["catch"](function (error) {
        if (error.response.status == 422) _this2.userErrors = error.response.data.errors;
      })["finally"](function (_) {
        return _this2.updating = false;
      });
    } // getUserDetails() {
    //   axios("api/user/address/billing").then(response => {
    //     const data = response.data;
    //     if (data) {
    //       this.address = { ...this.address, ...data };
    //       this.currentAddress = JSON.parse(JSON.stringify(data));
    //       this.getPlaces(data.district.id).then(
    //         response => (this.address.places = response.data)
    //       );
    //     }
    //     else {
    //       this.address.editMode = true;
    //     }        
    //     this.loading = false;
    //   });
    // },

  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Account.vue?vue&type=template&id=0b1dd512&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Account.vue?vue&type=template&id=0b1dd512& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "flex -mx-2" }, [
    _c("div", { staticClass: "flex-1 bg-white shadow-md rounded p-10 mx-2" }, [
      _c("h5", { staticClass: "text-lg font-serif uppercase mb-3" }),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "btn p-0 min-w-0 font-bold text-primary float-right",
          on: {
            click: function($event) {
              $event.preventDefault()
              _vm.user.editMode = !_vm.user.editMode
            }
          }
        },
        [
          _c("i", {
            class: ["fa", _vm.user.editMode ? "fa-times" : "fa-pencil-alt"]
          }),
          _vm._v(" " + _vm._s(_vm.user.editMode ? "Cancel" : "Change") + "\n  ")
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "clearfix" }),
      _vm._v(" "),
      _vm.user.editMode
        ? _c(
            "form",
            {
              attrs: { method: "POST" },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.updateUser($event)
                }
              }
            },
            [
              _c("div", { staticClass: "mb-3" }, [
                _c("b", [_vm._v(" Name")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.userForm.name,
                      expression: "userForm.name"
                    }
                  ],
                  staticClass: "form-control text-lg",
                  attrs: { type: "text", placeholder: "Name" },
                  domProps: { value: _vm.userForm.name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.userForm, "name", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.userErrors["name"]
                  ? _c(
                      "span",
                      {
                        staticClass: "text-red font-medium block",
                        attrs: { role: "alert" }
                      },
                      [_vm._v(_vm._s(_vm.userErrors["name"][0]))]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("b", [_vm._v("Phone No.")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.userForm.phone_number,
                      expression: "userForm.phone_number"
                    }
                  ],
                  staticClass: "form-control text-lg",
                  attrs: { type: "text", placeholder: "Phone Number" },
                  domProps: { value: _vm.userForm.phone_number },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.userForm,
                        "phone_number",
                        $event.target.value
                      )
                    }
                  }
                }),
                _vm._v(" "),
                _vm.userErrors["phone_number"]
                  ? _c(
                      "span",
                      {
                        staticClass: "text-red font-medium block",
                        attrs: { role: "alert" }
                      },
                      [_vm._v(_vm._s(_vm.userErrors["phone_number"][0]))]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("b", [_vm._v(" Father's Name ")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.userForm.father,
                      expression: "userForm.father"
                    }
                  ],
                  staticClass: "form-control text-lg",
                  attrs: { type: "text", placeholder: "Father Name" },
                  domProps: { value: _vm.userForm.father },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.userForm, "father", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.userErrors["father"]
                  ? _c(
                      "span",
                      {
                        staticClass: "text-red font-medium block",
                        attrs: { role: "alert" }
                      },
                      [_vm._v(_vm._s(_vm.userErrors["father"][0]))]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("b", [_vm._v(" Mother's Name")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.userForm.mother,
                      expression: "userForm.mother"
                    }
                  ],
                  staticClass: "form-control text-lg",
                  attrs: { type: "text", placeholder: "Mother Name" },
                  domProps: { value: _vm.userForm.mother },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.userForm, "mother", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.userErrors["mother"]
                  ? _c(
                      "span",
                      {
                        staticClass: "text-red font-medium block",
                        attrs: { role: "alert" }
                      },
                      [_vm._v(_vm._s(_vm.userErrors["mother"][0]))]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("b", [_vm._v(" Wife's Name")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.userForm.wife,
                      expression: "userForm.wife"
                    }
                  ],
                  staticClass: "form-control text-lg",
                  attrs: { type: "text", placeholder: "Wife Name" },
                  domProps: { value: _vm.userForm.wife },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.userForm, "wife", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.userErrors["wife"]
                  ? _c(
                      "span",
                      {
                        staticClass: "text-red font-medium block",
                        attrs: { role: "alert" }
                      },
                      [_vm._v(_vm._s(_vm.userErrors["wife"][0]))]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("b", [_vm._v(" Child's Name")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.userForm.child,
                      expression: "userForm.child"
                    }
                  ],
                  staticClass: "form-control text-lg",
                  attrs: { type: "text", placeholder: "Child" },
                  domProps: { value: _vm.userForm.child },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.userForm, "child", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.userErrors["child"]
                  ? _c(
                      "span",
                      {
                        staticClass: "text-red font-medium block",
                        attrs: { role: "alert" }
                      },
                      [_vm._v(_vm._s(_vm.userErrors["child"][0]))]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("b", [_vm._v(" Address")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.userForm.address,
                      expression: "userForm.address"
                    }
                  ],
                  staticClass: "form-control text-lg",
                  attrs: { type: "text", placeholder: "Address" },
                  domProps: { value: _vm.userForm.address },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.userForm, "address", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.userErrors["address"]
                  ? _c(
                      "span",
                      {
                        staticClass: "text-red font-medium block",
                        attrs: { role: "alert" }
                      },
                      [_vm._v(_vm._s(_vm.userErrors["address"][0]))]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mt-6" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary float-right",
                    attrs: { type: "submit", disabled: _vm.updating }
                  },
                  [_vm._v("Update")]
                )
              ])
            ]
          )
        : _c("div", { staticClass: "text-lg" }, [
            _c("div", { staticClass: "uppercase font-bold text-sm" }, [
              _c("b", [_vm._v(" Name ")]),
              _vm._v(":  " + _vm._s(_vm.user.name))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "uppercase font-bold text-sm mt-3" }, [
              _c("b", [_vm._v(" Email ")]),
              _vm._v(": " + _vm._s(_vm.user.email))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "uppercase font-bold text-sm mt-3" }, [
              _c("b", [_vm._v(" Phone Number ")]),
              _vm._v(": " + _vm._s(_vm.user.phone_number))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "uppercase font-bold text-sm mt-3" }, [
              _c("b", [_vm._v(" Father Name ")]),
              _vm._v(": " + _vm._s(_vm.userdetails.father))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "uppercase font-bold text-sm mt-3" }, [
              _c("b", [_vm._v(" Mother Name ")]),
              _vm._v(": " + _vm._s(_vm.userdetails.mother))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "uppercase font-bold text-sm mt-3" }, [
              _c("b", [_vm._v(" Wife Name ")]),
              _vm._v(": " + _vm._s(_vm.userdetails.wife))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "uppercase font-bold text-sm mt-3" }, [
              _c("b", [_vm._v(" Child Name ")]),
              _vm._v(": " + _vm._s(_vm.userdetails.child))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "uppercase font-bold text-sm mt-3" }, [
              _c("b", [_vm._v(" Address ")]),
              _vm._v(": " + _vm._s(_vm.userdetails.address))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-lg-6 col-md-6" }, [
              _c(
                "div",
                {
                  staticClass: "avatar img-fluid img-circle",
                  staticStyle: { "margin-top": "10px" }
                },
                [_c("img", { attrs: { src: _vm.user.avtar } })]
              )
            ])
          ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Account.vue":
/*!*********************************************!*\
  !*** ./resources/js/components/Account.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Account_vue_vue_type_template_id_0b1dd512___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Account.vue?vue&type=template&id=0b1dd512& */ "./resources/js/components/Account.vue?vue&type=template&id=0b1dd512&");
/* harmony import */ var _Account_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Account.vue?vue&type=script&lang=js& */ "./resources/js/components/Account.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Account_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Account_vue_vue_type_template_id_0b1dd512___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Account_vue_vue_type_template_id_0b1dd512___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Account.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Account.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/components/Account.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Account_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Account.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Account.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Account_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Account.vue?vue&type=template&id=0b1dd512&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/Account.vue?vue&type=template&id=0b1dd512& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Account_vue_vue_type_template_id_0b1dd512___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Account.vue?vue&type=template&id=0b1dd512& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Account.vue?vue&type=template&id=0b1dd512&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Account_vue_vue_type_template_id_0b1dd512___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Account_vue_vue_type_template_id_0b1dd512___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);